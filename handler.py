import webapp2
import jinja2
import hashlib, hmac
import random
import os

################## SECURITY ########################   

secret = '32be61f0bac69b623b90c8b0187991224eefa5c5271e6e5d3012be5cee2235ad'

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                                autoescape = True)


def make_secure_val(s):
    return "%s|%s" % (s, hmac.new(secret, s).hexdigest())

def check_secure_val(h):
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val

def make_salt(length = 5):
    return ''.join([random.choice(string.letters) for i in xrange(length)])

def make_pw_hash(name, password, salt=None):
    if salt is None:
        salt = make_salt()
    #this is stored in db
    return "%s|%s" % (hashlib.sha256(name+password+salt).hexdigest(),salt)

def valid_pw(name, password, h):
    t = make_pw_hash(name, password, h.split("|")[1])
    return t == h

class Handler(webapp2.RequestHandler):
    CACHE_USERNAMES = {}
    
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
        
    def set_secure_cookie(self, name, val):
        "sets secure cookie header"
        cookie_val = make_secure_val(val)
        #no expire value, expires after closing the browser
        self.response.headers.add_header(
            'Set-Cookie', '%s=%s; Path=/' % (name, cookie_val))
    
    def read_secure_cookie(self, name):
        #finds cookie in request, and return either cookie or false
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)
    
    def login(self, user):
        self.set_secure_cookie('user_id', str(user.key().id()))
    
    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')

    def initialize(self, *a, **kw):
        """called before every request, first checks for secure cookie, then 
        #if exists, store this user id"""
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id')
        self.user = uid and User.by_id(int(uid))