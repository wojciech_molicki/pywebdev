from handler import Handler
import time
import string
import json
import re
import logging

from google.appengine.ext import db
from google.appengine.api import memcache

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASS_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
   
class BlogEntries(db.Model):
    title = db.StringProperty(required = True)
    body = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)

    def get_content(self):
        return self.body.replace('\n', '<br />')

class BlogFrontPage(Handler):
    def render_front(self):
        posts, query_time = self.front_page()
        queried_time = int(time.time()) - query_time
        
        self.render("blog_front.html", posts = posts, queried_time = queried_time)

    def get(self):
        logging.error("GET F")
        self.render_front()

    def front_page(self, update = False):
        key = 'blog_front'
        r = memcache.get(key)
        #r = None
        if r is None or update:
            logging.error("DB QUERY, UPDATE MEMCACHE")
            posts = db.GqlQuery("SELECT * FROM BlogEntries ORDER BY created DESC LIMIT 10")
            posts = list(posts)

            t = int(time.time())
            memcache.set(key, (posts, t))
            memcache.flush_all()
        else:
            posts, t = r
        return posts, t

class NewPost(BlogFrontPage):
    def render_front(self, title = "", body = "", error = ""):
        self.render("blog_newpost.html", title = title, body = body, error = error)

    def get(self):
        self.render_front()

    def post(self):
        title = self.request.get("subject")
        body = self.request.get("content")
        if title and body:
            p = BlogEntries(title = title, body = body)
            p.put()
            self.front_page(update = True)
            self.redirect("/blog/%s" % str(p.key().id()))
        else:
            error = "You need to put in both title and body!"
            self.render_front(title, body, error)


class PermaLink(Handler):
    def render_front(self, apost, queried_time):
        self.render("blog_permalink.html", apost = apost, queried_time = queried_time)

    def get(self, _id):
        apost, t = self.permalink(_id)
        queried_time = int(time.time()) - t
        self.render_front(apost, queried_time)

    def permalink(self, _id):
        key = str(_id)
        r = memcache.get(key)

        if r:
            apost, t = r
        else:
            apost = BlogEntries.get_by_id(int(_id))
            #apost = list(apost)
            t = int(time.time())
            memcache.set(key, (apost, t))
        return apost, t

class Signup(Handler):
    errors = {'username_error' : '',
    'match_error' : '',
    'password_error' : '',
    'email_error' : '' }

    username = ''
    email = ''

    def get(self):
        self.render("signup.html", username = self.username, email = self.email,
         errors = self.errors)

    def post(self):
        self.username = self.request.get("username")
        self.email = self.request.get("email")
        password = self.request.get("password")
        verify = self.request.get("verify")

        if not self.check_errors(self.username, self.email, password, verify):
            #check in db
            user = User.by_name(self.username)
            if user:
                msg = "That user already exists"
                self.errors['username_error'] = msg
                self.render("signup.html", errors = self.errors)
            else:  
                #store into db
                user = User.register(self.username, password, self.email) 
                user.put()

                self.login(user)                                      
                self.redirect("/blog/welcome")
        else:
            self.render("signup.html", username = self.username, email = self.email,
         errors = self.errors )

    #regex matching
    def username_valid(self, username):
        return USER_RE.match(username)

    def email_valid(self, email):
        return EMAIL_RE.match(email)

    def password_valid(self, password):
        return PASS_RE.match(password)
    
    def check_errors(self, username, email, password, verify):
        error = False
        if not self.username_valid(username):
            self.errors['username_error'] = "That's not a valid username."
            error = True
        if not self.password_valid(password):
            self.errors['password_error'] = "That's not a valid password."
            password = ''
            error = True
        if password != verify:
            self.errors['match_error'] = "Your passwords didn't match."
            password = ''
            verify = ''
            error = True
        if (not self.email_valid(email)) and email != '':
            self.errors['email_error'] = "That's not a valid email."
            error = True
        return error

class Welcome(Handler):
    def get(self):
        #user is set in Handler initialize function
        if self.user:
            self.render('welcome.html', username = self.user.username)
        else:
            self.redirect("/blog/signup")
            
class Login(Handler):
    def get(self):
        self.render("login.html", login_error = '')
    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        
        user = User.login(username, password)
        if user:
            self.login(user)
            self.redirect("/blog/welcome")
        else:
            self.render("login.html", login_error = "Invalid login error.")

class Logout(Handler):
    """redirects to signup page, clears cookies"""
    def get(self):
        self.user = None
        self.logout()
        self.redirect("/blog/signup")

class FlushCache(Handler):
    """flushes all cache, and redirects to front page"""
    def get(self):
        memcache.flush_all()
        self.redirect("/blog")


################### DATABASE TABLES #################



class User(db.Model):
    username = db.StringProperty(required = True)
    password = db.StringProperty(required = True)
    email = db.StringProperty()
    created = db.DateTimeProperty(auto_now_add = True)
    
    #decorators, functions called on object
    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(uid)
    
    @classmethod
    def by_name(cls, name):
        user = User.all().filter('username =', name).get()
        return user
    
    @classmethod
    def register(cls, name, pw, email = None):
        pw_hash = make_pw_hash(name, pw)
        return User(username = name, password = pw_hash, email = email)

    @classmethod
    def login(cls, name, pw):
        user = cls.by_name(name)
        if user and valid_pw(name, pw, user.password):
            return user
            
class JsonHandler(Handler):
    def render_json(self):
        posts = db.GqlQuery("SELECT * FROM BlogEntries ORDER BY created DESC LIMIT 10")
        l = [self.dataobj_to_json(i) for i in posts]
        x = json.dumps(l)
        self.response.headers['Content-Type'] = "application/json, charset=UTF-8"
        self.write(x)

    def get(self):
        self.render_json()

    def dataobj_to_json(self, obj):
        return {"content":obj.body, "created": obj.created.strftime('%d %B %Y %H:%M:%S'), "subject": obj.title}

class PermaLinkJson(JsonHandler):
    def get(self, _id):
        apost = BlogEntries.get_by_id(int(_id))
        self.response.headers['Content-Type'] = "application/json, charset=UTF-8"
        x = self.dataobj_to_json(apost)
        self.write(json.dumps(x))