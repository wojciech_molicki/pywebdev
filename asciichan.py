from handler import Handler
from xml.dom import minidom
from google.appengine.ext import db
from google.appengine.api import memcache
import logging
import time
import urllib2

IP_URL = "http://freegeoip.net/xml/"

def get_coords(ip):
    url = IP_URL + ip
    #storing content of url
    content = None
    try:
        content = urllib2.urlopen(url).read()
    except urllib2.URLError:
        return

    if content:
        #parse xml and find coordinates
        return coords_from_xml(content)


def coords_from_xml(xml):
    dom = minidom.parseString(xml)
    lat = dom.getElementsByTagName("Latitude")
    lon = dom.getElementsByTagName("Longitude")
    if (lat and lon) and (lat[0].firstChild.nodeValue and lon[0].firstChild.nodeValue):
        #coords = str(c[0].firstChild.nodeValue)
        #lon, lat = coords.split(',')
        #appengine geo point format
        return db.GeoPt(lat[0].firstChild.nodeValue, lon[0].firstChild.nodeValue)

def gmaps_img(points):
    "takes list of points and returns url with points marker params"
    GMAPS_URL = "http://maps.googleapis.com/maps/api/staticmap?size=380x264&sensor=false&"
    return GMAPS_URL + '&'.join(['markers=%f,%f' % (p.lat, p.lon) for p in points])

def top_arts(update = False):
    #cache key
    key = 'top'

    #memcache, both key and values => strings
    arts = memcache.get(key)

    if arts is None or update:
        #prints to console everytime db is queried
        logging.error("DB QUERY")

        #this query needs to be cached, so we can limit the amount of db hits
        arts = db.GqlQuery("SELECT * FROM Art ORDER BY created DESC LIMIT 10")

        #make new list from iterable, so query wont run more 
        #than once here
        arts = list(arts)
        
        #set memcache
        memcache.set(key, arts)
    return arts

class Art(db.Model):
    #creating db fields
    title = db.StringProperty(required = True)
    art = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    coords = db.GeoPtProperty()
    ip = db.StringProperty()

class AsciiChan(Handler):
    def render_front(self, title = "", art = "", error = ""):

        arts = top_arts()

        #lists all coords which are not none
        points = filter(None, [a.coords for a in arts])
        #self.write(repr(points))

        img_url = None
        #if there are some coords, display image url
        if points:
            img_url = gmaps_img(points)
            

        self.render("front.html", title = title, art = art,
         error = error, arts = arts, img_url = img_url)

    def get(self):
        #some testing
        #self.write(repr(get_coords('8.8.8.8')))
        self.render_front()

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")

        if title and art:
            a = Art(title = title, art = art)
            a.ip = self.request.remote_addr
            #lookup users coordinates from user ip
            coords = get_coords(self.request.remote_addr)
            if coords:
                a.coords = coords
            #stores new Art obj in database
            a.put() 

            #clear the cache after submitting
            #CACHE.clear()

            #rerun query and update cache
            top_arts(update = True)

            self.redirect("/ascii")
        else :
            error = "You need to put in both art ant title!"
            self.render_front(title, art, error)

