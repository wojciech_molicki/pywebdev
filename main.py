import webapp2
import blog
import logging

from handler import Handler
from asciichan import AsciiChan

class MainPage(Handler):
    def get(self):
        logging.error("MAIN")
        self.render("mainpage.html")

app = webapp2.WSGIApplication([
    ('/?', MainPage),
    ('/ascii/?', AsciiChan),
    ('/blog/signup/?', blog.Signup),
    ('/blog/login/?', blog.Login),
    ('/blog/logout/?', blog.Logout),
    ('/blog/flush/?', blog.FlushCache),
    ('/blog/welcome/?', blog.Welcome),
    ('/blog/?', blog.BlogFrontPage),
    ('/blog/?\.json', blog.JsonHandler),
    ('/blog/newpost/?', blog.NewPost), 
    ('/blog/([0-9]+)/?', blog.PermaLink),
    ('/blog/([0-9]+)\.json', blog.PermaLinkJson)],
     debug=True)